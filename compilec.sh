#!/bin/bash

rm "${1}.hex"
rm "${1}.out"
avr-gcc -mmcu=atmega2560 "${1}.c" utils/* -O -o "${1}.out"
avr-objcopy -O ihex -R .eeprom "${1}.out" "${1}.hex"
