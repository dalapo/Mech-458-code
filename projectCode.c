#include<avr/io.h>

/*
 * We have divided up the code into a number of separate files in order to organize it for ourselves, and
 * for ease of testing and troubleshooting, as well as to encapsulate as much as possible (so that once an element
 * "just works", it will "just work" for everything without needing further tweaking depending on the context).
 *
 * All of these files can be found in the utils folder.
 */
#include"utils/stepMotor.h"
#include"utils/lcd.h"
#include"utils/interrupts.h"
#include"utils/timer.h"
#include"utils/pwm.h"
#include"utils/adc.h"
#include"utils/LinkedList.h"
#include"utils/misc.h"

// The minimum and maximum ADC readings for black, aluminum, white, and steel objects, in that order
const int THRESHOLDS[4][2] = {{956, 1023}, {0, 255}, {900, 955}, {400, 700}};

// The names of the objects (for LCD purposes), in the same order as the above array
const char* NAMES[] = {"Black", "Aluminum", "White", "Steel"};

// Number of sorted objects of each type. In order: Unsorted, Black, Aluminum, White, Steel
int sortedCounts[5] = {0, 0, 0, 0, 0};

const int DELTA = 5; // Minimum amount that reading must exceed lowest by to stop scanning
const int RAMPDOWN_DELAY = 7000; // Delay between rampdown button press and rampdown sequence
const char CONVEYOR_DUTY_CYCLE = 0x60; // Duty cycle for the conveyor PWM. 0x60 is roughly the max DC motor speed.

// A 90 degree rotation of the bucket step motor is 50 steps. 
const int STEP_LENGTH = 50;

// State flags. Implemented like this instead of using a state machine to account for the possibility of
// multiple flags being active at once.
volatile char isRunning = false;
volatile char isMeasuring = false;
volatile char isReorienting = false;
volatile char isRampdown = false;
volatile char isPaused = false;

/*
 * Given a raw ADC value from 0 to 1023, compares it to the numbers in the THRESHOLDS array and returns a corresponding
 * number from 0-3 corresponding to the type of material. Order is black, aluminum, white, steel
 */
unsigned char getMaterial(int in)
{
	for (uchar i=0; i<4; i++)
	{
		if (in >= THRESHOLDS[i][0] && in <= THRESHOLDS[i][1]) return i;
	}
	return 255;
}

// Returns the human-readable representation of the material name
const char* getName(unsigned char id)
{
	if (id >= 0 && id < 4) return NAMES[id];
	return "Unknown";
}

// Displays the number of sorted objects and their types on the LCD
void displaySorted()
{
	LCDClear();
	LCDWriteString("X B A W S");
	LCDGotoXY(0, 1);
	for (int i=0; i<5; i++)
	{
		LCDWriteInt(sortedCounts[i], 1);
		LCDWriteString(" ");
	}
}

// Convenience method to stop the conveyor
void stopConveyor()
{
	PORTB = 0x0F;
}

// Convenience method to start the conveyor
void startConveyor()
{
	setDutyCycle(CONVEYOR_DUTY_CYCLE);
	PORTB = 0x07;
}

// INT5 (Ramp down)
void rampdown()
{
	TCCR1B |= _BV(CS11) | _BV(WGM32);
	OCR1A = 1000;
	TCNT1 = 0x0000;
	TIMSK1 |= 0x02;
}

// INT4 (Start)
void start()
{
	isRunning = true;
}

// INT4 (Pause)
void togglePause()
{
	mTimer(1);
	if ((PIND & 0x10) == 0) isPaused = !isPaused;
}

// INT1 (Homing)
void onBlack()
{
	isReorienting = false;
}

// INT2 (OR sensor)
void startMeasuring()
{
	mTimer(1);
	if ((PIND & 0x04) != 0) isMeasuring = true;
}

// INT3 (EX sensor)
void onObjectReachedEnd()
{
	mTimer(1);
	if ((PIND & 0x08) == 0) isReorienting = true;
}

// Main loop
int main(int argc, char** argv)
{
	initTimer();

	DDRC = 0xFF; // LEDs, LCD (output)
	DDRB = 0xFF; // DC Motor
	DDRA = 0xFF; // Step motor
	DDRD = 0x00; // Interrupt sensors
	DDRE = 0x00; // Buttons

	// Initialize variables used in ADC reading
	int lowest = 1024;
	int reading = -1;

	// Initialize the queue (see LinkedList.c)
	LinkedList* queue = getList(); 

	initPWM(); // See utils/pwm.c
	setWaveGen(WGM_FPWM);

	initADC(); // see utils/adc.c
	// HE: 1, active low
	// OR: 2, active high
	// EX: 3, active low
	// Start/Pause: 4, active low
	// Rampdown: 5, active low
	initInterrupts(0x7E, 0x04); // Init interrupts 1, 2, 3, 4, and 5. Interrupt 2 is active high.
	initLCD();
	LCDClear();

	/*
	 * Assign the interrupt-triggered functions to their corresponding ISR vectors. This implementation uses
	 * function pointers; see utils/interrupts.c
	 */
	assignFunction(1, onBlack);
	assignFunction(2, startMeasuring);
	assignFunction(3, onObjectReachedEnd);
	assignFunction(4, start);
	assignFunction(5, rampdown);

	LCDWriteString("Press start");
	while (!isRunning) {}; // Wait for start button press

	LCDClear();
	LCDGotoXY(0, 0);
	LCDWriteString("Orienting...");
	isReorienting = true;

	// Rotate the motor until it reaches black, then reset the step counter. We don't want to accelerate for this.
	while (isReorienting)
	{
		step();
		mTimer(20);
	}
	resetMotor();

	assignFunction(1, nop); // Disable the onBlack interrupt, we don't want it firing while things are being sorted
	assignFunction(4, togglePause); // Reassign INT4 from start to pause

	startConveyor();

	initAccelProfile();

	LCDClear();
	LCDWriteString("Starting");

	// Main loop starts here
	while (true)
	{
		/*
		 * If program is paused, stop the conveyor and display the sorted objects
		 * Wait in an infinite loop until the pause button is pressed again, then start the conveyor
		 * as long as the bucket isn't moving.
		 */
		if (isPaused)
		{
			stopConveyor();
			LCDClear();
			displaySorted();
			while (isPaused) {}
			if (!isReorienting) startConveyor();
		}

		/*
		 * This code runs if an object is at the exit sensor and reorients the basket to sort it properly.
		 * Stop the conveyor and fetch the object ID at the head of the queue to know what position to
		 * put the bucket into. Then rotate the bucket accordingly and restart the conveyor.
		 *
		 * Relevant util files: LinkedList.c, stepMotor.c
		 */
		if (isReorienting) // Rotating basket
		{
			stopConveyor();
			char c = dequeue(queue);
			if (c >= 0 && c < 4) goToPosAccel(c * (STEP_LENGTH));
			isReorienting = false;
			sortedCounts[c+1] = sortedCounts[c+1] + 1;
			startConveyor();
		}

		/*
		 * This code runs if an object has triggered the OR sensor. Poll the reflectivity sensor and
		 * keep track of the lowest returned value for this object. If the latest reading is more than 5
		 * higher than the lowest, the object is on its way out of the sensor and we don't need to
		 * track it anymore. Find the part ID from the lowest reading and push that onto the queue, and display
		 * the reading and name on the LCD.
		 *
		 * Relevant util files: adc.c, LinkedList.c
		 */
		else if (isMeasuring) // Sensor polling
		{
			reading = getADC10Bit();
			if (reading < lowest) lowest = reading;
			LCDClear();
			LCDWriteInt(lowest, 4);
			LCDGotoXY(0, 1);
			LCDWriteInt(reading, 4);
			if (reading - lowest >= DELTA)
			{
				unsigned char c = getMaterial(lowest);
				push(queue, c);
				reading = 0;
				lowest = 1024;
				isMeasuring = false;
			}
		}
		sortedCounts[0] = queue->size; // queue->size equals the number of unsorted objects

		/*
		 * This code runs if rampdown is set and there are no unsorted objects.
		 * It stops the conveyor, then displays the sorted counts.
		 */
		if (isRampdown && queue->size == 0)
		{
			stopConveyor();
			while (true)
			{
				displaySorted();
				mTimer(1000);
				LCDClear();
				LCDWriteString("Dave & Jack");
				LCDGotoXY(0, 1);
				LCDWriteString("Are Great");
				mTimer(1000);
			} // Endless loop
		}
	}
}

/*
 * This interrupt becomes active once the rampdown button is pressed.
 * It sets the rampdown state to active once the 7000 ms delay has elapsed.
 */
volatile int ms = 0;
ISR(TIMER1_COMPA_vect)
{
	if (++ms >= RAMPDOWN_DELAY) isRampdown = true;
}
