#ifndef PWM_H_INCL
#define PWM_H_INCL

#include<avr/io.h>
#include<avr/interrupt.h>

/*
#define WGM_NORMAL 0x00
#define WGM_PWMPC8 0x01
#define WGM_PWMPC9 0x02
#define WGM_PWMPC10 0x03
#define WGM_MCTC 0x04
#define WGM_PWMF8 0x05
#define WGM_PWMF9 0x06
#define WGM_PWMF10 0x07
#define WGM_PWMPFI 0x08
#define WGM_PWMPFO 0x09
#define WGM_PWMPCI 0x0A
#define WGM_PWMPCO 0x0B
#define WGM_CTC 0x0C
// 0x0D is reserved
#define WGM_PWMFI 0x0E
#define WGM_PWMFO 0x0F
*/

#define WGM_NORMAL 0x00 // Normal operation, no PWM
#define WGM_PCPWM 0x01 // PWM, Phase correct
#define WGM_0CTC 0x02 // CTC
#define WGM_FPWM 0x03 // Fast PWM, 0 to OxFF, signals at OCRA
// 0x04 is reserved
#define WGM_PCPWMO 0x05 // PWM, Phase correct, OCRA to 0xFF, signals at OCRA (I think)
// 0x06 is reserved
#define WGM_FPWMO 0x07 // Fast PWM, OCRA to 0xFF, signals at OCRA (I think)

void setWaveGen(char wave);
void setDutyCycle(char val);
void initPWM();
char getWaveGen();

#endif
