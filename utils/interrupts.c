#include<avr/io.h>
#include<avr/interrupt.h>
#include"interrupts.h"
#include"timer.h"

/**
 * This source file sets up and abstracts interrupts.
 * The ISR subroutines each call a function pointer in the functions[] array.
 * Functions can be defined in a program's main source file and then assigned, thereby
 * eliminating the need to directly define the ISRs elsewhere.
 */

void nop() {} // No-operation function, default assigned function for interrupts

/*
 * Initialize the interrupts. active and rising are two 8-bit registers specifying which interrupts are used, and of those
 * which are rising edge. A given bit in active should be set to 1 if its corresponding interrupt should be active.
 * Likewise, if a given bit in rising is 1, its interrupt will activate on a rising edge. Otherwise, it will activate
 * on a falling edge.
 */
void initInterrupts(char active, char rising)
{
	// Initialize all function pointers to the nop function; it is assumed that these will be reassigned using
	// assignFunction later
	for (int i=0; i<8; i++)
	{
		functions[i] = nop;
	}

	cli();

	// EIMSK is the register that specifies which interrupts are active, and uses the same format as
	// the passed-in active variable.
	EIMSK = active;

	// Set all interrupts to falling-edge to start
	EICRA = 0x00;

	/*
	 * The rising parameter defines which interrupts are rising-edge (1) and falling-edge (0).
	 * The following bit of code properly sets the EICRA and EICRB registers accordingly.
	
	 * EICRA and EICRB use 2 bits for each interrupt. Bits 1, 3, 5, and 7 should always be set to 1
	 * in order to have either rising-edge or falling-edge behaviour. Bits 0, 2, 4, and 6 should be low
	 * for falling edge and high for rising edge.
	 *
	 * To properly get the bits from rising into EICRA and EICRB, we right-shift rising one step at a time
	 * and take the result & 1 to get either a 1 or a 0 on its own. We then left-shift it back two steps at a time
	 * to put it properly into EICRA.
	 *
	 * While writing this comment, I realized that you could just start by initalizing EICRA and EICRB
	 * to 0b10101010 and avoid the first line of the for loop. Oh well.
	 */
	for (int i=0; i<4; i++)
	{
		EICRA |= (1 << (2*i+1)); // Set bits 1, 3, 5, 7 to 1
		EICRA |= ((rising >> i) & 1) << (2 * i); // Holy bit-shifting, batman!
	}
	char b = (rising >> 4) & 0x0F; // Take just the high half of rising to use for EICRB
//	EICRB &= 0xF0;
	EICRB = 0x00;
	for (int i=0; i<4; i++) // Same as above but with EICRB
	{
		EICRB |= (1 << (2*i+1));
		EICRB |= ((b >> i) & 1) << (2 * i);
	}
	sei();
}

// Sets interrupt i to call the corresponding function pointer
void assignFunction(int i, void (*func)())
{
	if (i >= 0 && i < 8) functions[i] = func;
}

/*
 * The actual interrupt subroutines are all defined here. Each of them calls the corresponding function pointer.
 * Note that there is no risk of undefined behaviour here because the same function above that activates
 * the interrupts also sets each of these functions to nop().
 */
ISR(INT0_vect)
{
	(*functions[0])();
}

ISR(INT1_vect)
{
	(*functions[1])();
}

ISR(INT2_vect)
{
	(*functions[2])();
}

ISR(INT3_vect)
{
	(*functions[3])();
}

ISR(INT4_vect)
{
	(*functions[4])();
}

ISR(INT5_vect)
{
	(*functions[5])();
}

ISR(INT6_vect)
{
	(*functions[6])();
}

ISR(INT7_vect)
{
	(*functions[7])();
}

// Bad ISR handler; just do nothing.
ISR(BADISR_vect)
{
	nop();
}
