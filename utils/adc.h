#ifndef ADC_H_INCL
#define ADC_H_INCL

#include<avr/io.h>
#include<avr/interrupt.h>

volatile unsigned char adc;
volatile unsigned int adc10;
volatile unsigned char is10bit;
volatile unsigned char adc_flag;

void initADC();
char getADC();
int getADC10Bit();

#endif
