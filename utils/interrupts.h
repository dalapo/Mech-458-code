#ifndef INTERRUPT_H_INCL
#define INTERRUPT_H_INCL
#include<avr/io.h>
#include<avr/interrupt.h>

void initInterrupts(char, char); // Initializes interrupts
void assignFunction(int i, void (*func)()); // Assigns the passed function to the passed interrupt ID
void nop(); // Default function for interrupt subroutines; does nothing

void (*functions[8])(); // Function pointer array. functions[i] is called from ISR(INTi_vec)

#endif
