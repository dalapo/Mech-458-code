#include<avr/io.h>
#include<avr/interrupt.h>
#include"stepMotor.h"
#include"timer.h"
#include"lcd.h"

/*
 * stepMotor.c contains functions for the operation of stepper motors. This includes step counting, acceleration profile,
 * and going to a predetermined position.
 */

// Steps per revolution.
// const int STEPS_PER_REV = 2048; // For our little motor
const int STEPS_PER_REV = 200; // For the main apparatus motor

const int BASE_SPS = 58; // Minimum speed, 17 ms/step
const int MAX_SPS = 166; // Maximum speed, 6 ms/step
const int MS_TO_MAX = 120; // Time (ms) to go from min speed to max

volatile unsigned char next = 0; // The next truth table index to pulse
volatile int steps = 0; // The number of steps away from home the motor is
volatile int accelDirection = 0; // 1: Accelerating / 0: Constant speed / -1: Decelerating

volatile unsigned long spsRough; // Intermediate value used in calculations to avoid integer division nastiness
volatile unsigned long dOmega; // delta-omega by delta-t, x 1000.
volatile int sps = 50; // steps per second of the motor

// Math:
// a = dv/dt, where dv = (MAX_SPS - BASE_SPS) and dt = msToMax

//const char stepTable[] = {0b0011, 0b0110, 0b1100, 0b1001}; // Truth table for our little motor
const char stepTable[] = {0b110110, 0b101110, 0b101101, 0b110101}; // Truth table for the main apparatus motor

// Sets the SPS of the motor
void setRate(int s)
{
	sps = s;
}

/*
 * initAccelProfile initializes the timer used to control acceleration, the value of spsRough,
 * and the value of dOmega. See the interrupt handler at the bottom of the file for more information.
 */
void initAccelProfile()
{
	TCCR5B |= _BV(CS11) | _BV(WGM32);
	OCR5A = 1000; // interrupt every ms
	TCNT5 = 0x0000;
	TIMSK5 |= 0x02;
	spsRough = BASE_SPS * 1000L;
	dOmega = (1000L * (MAX_SPS - BASE_SPS)) / MS_TO_MAX;
	sei();
}

// Resets the step counter to zero. Used in the main program when the black homing sensor is triggered.
void resetMotor()
{
	steps = 0;
}

// Takes one step forwards
void step()
{
	DDRA |= 0x0F;
	next = (next + 1) & 0x03; // Go to the next step in the truth table. Take next & 0x03 to keep it in the range of 0:3.
	PORTA = stepTable[next];
	steps++;
	steps %= STEPS_PER_REV;
}

// Takes one step backwards.
void stepReverse()
{
	DDRA |= 0x0F;
	next = (next - 1) & 0x03; // Go to the previous step in the truth table. Take next & 0x03 to keep it in the range of 0:3.
	// Note: 0x00 - 1 = 0xFF; 0xFF & 0x03 = 0x03
	PORTA = stepTable[next];
	steps--;
	steps %= STEPS_PER_REV;
}

// Rotate the motor s steps, with the trapezoidal acceleration profile.
// If s is negative, the motor is rotating backwards.
void rotateAccel(long s)
{
	// Absolute value of s; used in loop counts for accel and decel
	long absS = s < 0 ? -s : s;
	accelDirection = 1;

	// d = (v1+v0)*2/t. Divide by 1000 as well because the units of t is ms
	// integer division might become a problem, probably not though
	int stepsToMax = ((MAX_SPS + BASE_SPS) * MS_TO_MAX) / 2000;

	// The number of steps it takes to fully accelerate is a constant n
	// Once we reach (s-n) steps (that is, loop counter i == absS-n), start decelerating
	// If we are rotating fewer than 2n steps, don't accelerate fully, but accelerate half way and then decel
	// accelDirection is automatically set to 0 by the interrupt upon the motor reaching max speed.
	int stm = (absS > 2 * stepsToMax ? stepsToMax : absS / 2);
	if (s < 0)
	{
		for (int i=0; i<(-s); i++)
		{
			stepReverse();
			mTimer(1000 / sps);
			if (i >= (-s - stm)) accelDirection = -1; // Begin to decelerate at the appropriate time
		}
	}
	else
	{
		for (int i=0; i<s; i++)
		{
			step();
			mTimer(1000 / sps);
			if (i >= (s - stm)) accelDirection = -1; // Begin to decelerate at the appropriate time
		}
	}
}

// Rotates the motor at base speed. If s is negative, the motor is rotating backwards.
void rotate(long s)
{
	accelDirection = 0;
	if (s < 0)
	{
		for (int i=0; i<(-s); i++)
		{
			stepReverse();
			mTimer(1000 / sps);
		}
	}
	else
	{
		for (int i=0; i<s; i++)
		{
			step();
			mTimer(1000 / sps);
		}
	}
}

void rotateDeg(long deg) // Rotate a certain number of degrees.
{
	rotate((long)(deg * STEPS_PER_REV) / 360); // * 2048 / 360
}

void goToPos(int dest) // Rotate to a specific position, taking the shortest path.
{
	int dTheta = dest - steps;

	// If the motor would rotate more than 180 degrees, instead rotate the other direction.
	if (dTheta > (STEPS_PER_REV)/2) dTheta -= STEPS_PER_REV;
	else if (dTheta < -(STEPS_PER_REV)/2) dTheta += STEPS_PER_REV;
	rotate(dTheta);
}

void goToPosAccel(int dest) // As above, but with an acceleration profile.
{
	int dTheta = dest - steps;
	if (dTheta > (STEPS_PER_REV)/2) dTheta -= STEPS_PER_REV;
	else if (dTheta < -(STEPS_PER_REV)/2) dTheta += STEPS_PER_REV;
	rotateAccel(dTheta);
}

// interrupt runs every ms
ISR(TIMER5_COMPA_vect)
{
	/*
	 * A common problem we had while designing this acceleration profile was integer division.
	 * The way we solved it is by using an intermediate value, spsRough, to keep track of the motor's
	 * "true" steps per second, before dividing by 1000 and rounding down to the nearest integer.
	 *
	 * dOmega adds to or subtracts from spsRough, depending on whether the motor is currently accelerating or decelerating.
	 * If the motor has reached maximum or minimum speed, set accelDirection back to zero accordingly.
	 */
	if (MS_TO_MAX != 0) // Avoid div by zero
	{
		switch (accelDirection)
		{
			case 0:
				// If accelDirection is zero, run at constant speed
				break;
			case 1:
				if (sps < MAX_SPS) spsRough += dOmega;
				else accelDirection = 0;
				sps = (int)(spsRough / 1000);
				break;
			case -1:
				if (sps > BASE_SPS) spsRough -= dOmega;
				else accelDirection = 0;
				sps = (int)(spsRough / 1000);
				break;
		}
	}
}
