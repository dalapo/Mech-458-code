#include"timer.h"

/*
 * Utility file for the timer.
 * Contains convenience methods for initializing and setting the resolution of the timer, as well as the mTimer method.
 *
 * A possibility that is not implemented is using an interrupt to run a function in x amount of time, but without completely
 * pausing program execution like mTimer does.
 */
void initTimer()
{
	CLKPR = 0x80;
	CLKPR = 0x01;
	setRes(0x03E8);
	TCCR3B |= _BV(CS11);
}

void setRes(int res)
{
	OCR3A = res;
}

void mTimer(long count)
{
	long i = 0;
	TCCR3B |= _BV(WGM32);
	TCNT3 = 0x0000;
	TIFR3 |= _BV(OCF3A);
	
	while (i < count)
	{
		if ((TIFR3 & 0x02) == 0x02)
		{
			TIFR3 |= _BV(OCF3A);
			i++;
		}
	}
}	
