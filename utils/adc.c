#include<avr/io.h>
#include<avr/interrupt.h>
#include"adc.h"

/*
 * The utility file dedicated to the ADC
 */

/*
 * Initializes the ADC, taking analogue input from F1.
 */
void initADC()
{
	cli();
	ADCSRA |= 0x88; // ADEN | ADIE (Set the ADC Enable and ADC Interrupt Enable flags)
	ADCSRB &= 0xF7; // Clear MUX5
	ADMUX |= 0x61; // ADLAR | REFS0 | MUX0 (ADC with external capacitor; ADC left adjust; input pin F1)
	ADMUX &= 0xE1; // Clear MUX4:1
	sei();
	ADCSRA |= 0x40; // ADSC (Start conversion)
}

char getADC()
{
	is10bit = 0; // The ADC ISR will perform differently depending on this flag. Set it to zero to get an 8-bit output
	adc_flag = 0;
	ADCSRA |= 0x40; // Start conversion
	while (adc_flag != 1) {} // wait for interrupt to signal that ADC conversion is complete
	return adc;
}

int getADC10Bit()
{
	is10bit = 1; // Set this flag to 1 to indicate we want a 10-bit output
	adc_flag = 0;
	ADCSRA |= 0x40; // Start conversion
	while (adc_flag != 1) {} // wait for interrupt to signal that ADC conversion is complete
	return adc10;
}

/*
 * 8-bit ADC:
 * Ignore the two low bits, as in most 8-bit contexts range is more important than resolution.
 *
 * 10-bit ADC:
 * ADCH stores bits 9:2 as bits 7:0. ADCL stores bits 1:0 in its two highest bits.
 * We want to return a 10-bit number with ADC bits 9:0. Therefore,
 * we right shift ADCH by 2 (7:0 -> 9:2) and left shift ADCL by 6 (7:6->1:0).
 */
ISR(ADC_vect)
{
	if (is10bit == 0) adc = ADCH;
	else adc10 = (ADCL >> 6) + (ADCH << 2);
	adc_flag = 1;
}
