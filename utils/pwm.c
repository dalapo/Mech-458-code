#include<avr/io.h>
#include"pwm.h"

/*
 * The file for PWM functions.
 */

/*
 * Initializes the PWM to non-invert mode, running at 488 Hz (assuming a clock speed of 8MHz)
 */
void initPWM()
{
	TCCR0A |= 0xA0;	// 0b10100000, non-invert mode
	TCCR0B |= 0x03; // CS01 | CS00 for 488 Hz
	DDRB = 0xFF;
	DDRG = 0xFF;
}

/*
 * WGM2:0 are split up into 2 registers for some reason, and the bits aren't organized nicely, so this function
 * simplifies things. Sets the wave gen mode to one of 7 options (see pwm.h)
 */
void setWaveGen(char wave)
{
	// clear WGM2:0
	TCCR0A &= 0b11111100;
	TCCR0B &= 0b11110111;

	// set WGM1:0 in TCCR0A and WGM2 in TCCR0B according to the passed value
	// see pwm.h for defined modes
	TCCR0A |= (wave & 0x03);
	TCCR0B |= (wave & 0x04) << 1;
}

// Returns the wave gen in the same format as it's set in the function above
char getWaveGen()
{
	return (TCCR0A & 0x03) | ((TCCR0B & 0x08) >> 1);
}

// Sets the duty cycle of the PWM. 0x00 is completely low, 0xFF is completely high
void setDutyCycle(char val)
{
	OCR0B = val;
	if ((getWaveGen() & 0x05) != 0x05) OCR0A = val; // TOP is OCR0A iff WGM2 and WGM0 are both set
}


