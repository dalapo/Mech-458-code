#include<avr/io.h>
#include<avr/interrupt.h>
#include"utils/interrupts.h"
#include"utils/stepMotor.h"
#include"utils/timer.h"
#include"utils/lcd.h"

volatile long ms = 0;
volatile char dir = 1;

int main(int argc, char** argv)
{
	initTimer();
	DDRC = 0xFF;
	DDRD = 0x00;
	initLCD();
	LCDClear();
	initAccelProfile();

	TCCR1B |= _BV(CS11) | _BV(WGM32);
	OCR1A = 1000;
	TCNT1 = 0x0000;
	TIMSK1 |= 0x02;

	/*
	while (1)
	{
		ms = 0;
		rotateAccel(50 * dir);
		LCDWriteString("t(ms)");
		LCDGotoXY(0, 1);
		LCDWriteInt(ms, 4);
		LCDWriteString(" | ");
		LCDWriteInt(120, 3);
		mTimer(500);
		dir = -dir;
	}
	*/
	while (1)
	{
		goToPosAccel(50);
		mTimer(500);
		goToPosAccel(0);
		mTimer(500);
	}
}

ISR(TIMER1_COMPA_vect)
{
	ms++;
}
